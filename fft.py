'''
import numpy
# import pyaudio
# import pylab
import numpy
from scipy.io import wavfile as wav
import sys
import pygame
from pygame.locals import *
import datetime
import matplotlib.pyplot as plt

### RECORD AUDIO FROM MICROPHONE ###
# rate=44100
soundcard = 1  # CUSTOMIZE THIS!!!
filename = '/Users/oanceac/Downloads/Skrux-BelieveMe.wav'
rate, data = wav.read(filename)
fpsclock = pygame.time.Clock()

# p=pyaudio.PyAudio()
# strm=p.open(format=pyaudio.paInt16,channels=1,rate=rate,
# input_device_index=soundcard,input=True)
# strm.read(1024) #prime the sound card this way
# pcm=numpy.fromstring(strm.read(1024), dtype=numpy.int16)


### DO THE FFT ANALYSIS ###
print(data.shape)
fft = numpy.fft.fft2(data)
# fft = numpy.fft.rfft(fft2)
print(fft.shape, rate)
fft = fft.real
fft = fft.astype(numpy.int8)
print(fft.max(), fft.min())
fft = (fft + 128)//2
print(fft.dtype)
#print(fft[200000])
#plt.plot(fft[100:1000])
#plt.ylabel('some numbers')
#plt.show()
pygame.init()
pygame.mixer.init()
pygame.mixer.music.load(filename)
pygame.mixer.music.play()
print(fft.max(), fft.min())
i = 0
mili = 0
times = 0
time = datetime.datetime.now()


def fps():
    global time
    fps = datetime.datetime.now() - time
    fps = int(1000000 / fps.microseconds)
    time = datetime.datetime.now()
    return fps


while True:

    a = datetime.datetime.now()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.mixer.quit()
            sys.exit()
        elif event.type == KEYDOWN:
            if event.key == K_SPACE:
                pygame.mixer.quit()
                sys.exit()

    # msg1 = '#' * fft[i][0]
    # msg2 = '#' * fft[i][1]
    # print(msg1)
    # print(msg2)
    fpsclock.tick(40)
    frames = fps()
    i += int(44100/frames)
    #print(frames)
    print('#' * fft[i][0])
    # print('&' * fft[i][1])

# pygame.mixer.quit()
# fftr=10*numpy.log10(abs(fft.real))[:len(data)/2]
# ffti=10*numpy.log10(abs(fft.imag))[:len(data)/2]
# fftb=10*numpy.log10(numpy.sqrt(fft.imag**2+fft.real**2))[:len(data)/2]
# freq=numpy.fft.fftfreq(numpy.arange(len(data)).shape[-1])[:len(data)/2]
# freq=freq*rate/1000 #make the frequency scale


# -*- coding: utf-8 -*-
# reference:
# http://www.cnblogs.com/xiaowuyi/category/426566.html
# http://eyehere.net/2011/python-pygame-novice-professional-2/
# https://www.bilibili.com/video/av9615162/
# http://1029975378-qq-com.iteye.com/blog/1981053
# http://www.pygame.org/docs/ref/music.html
# https://docs.python.org/3/library/wave.html
# http://blog.csdn.net/daiyinger/article/details/48289575

'''

import sys, math, wave, numpy, pygame
from pygame.locals import *
from scipy.fftpack import dct

N = 30 # num of bars
HEIGHT = 100 # height of a bar
WIDTH = 10 # width of a bar
FPS = 10

file_name = '/Users/oanceac/Downloads/test.wav'
status = 'stopped'
fpsclock = pygame.time.Clock()

# screen init, music playback
pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode([N * WIDTH, 50 + HEIGHT])
pygame.display.set_caption('Audio Visulizer')
my_font = pygame.font.SysFont('consolas', 16)
pygame.mixer.music.load(file_name)
pygame.mixer.music.play()
pygame.mixer.music.set_endevent()
status = "playing"

# process wave data
f = wave.open(file_name, 'rb')
params = f.getparams()
nchannels, sampwidth, framerate, nframes = params[:4]
str_data  = f.readframes(nframes)
f.close()
wave_data = numpy.frombuffer(str_data, dtype = numpy.short)
wave_data.shape = -1,2
wave_data = wave_data.T



num = nframes

def visualizer(num):
    num = int(num)
    h = abs(dct(wave_data[0][nframes - num:nframes - num + N]))
    h = [min(HEIGHT,int(i **(1 / 2.5) * HEIGHT / 100)) for i in h]
    draw_bars(h)

def vis(status):
    global num
    if status == "stopped":
        num = nframes
        return
    elif status == "paused":
        visualizer(num)
    else:
        num -= framerate/FPS
        if num > 0:
            visualizer(num)

def get_time():
    seconds = max(0, pygame.mixer.music.get_pos()/1000)
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    hms = ('%02d:%02d:%02d' % (h, m, s))
    return hms

def controller(key):
    global status
    if status == 'stopped':
        if key == K_RETURN:
            pygame.mixer.music.play()
            status = 'playing'
    elif status == 'paused':
        if key == K_RETURN:
            pygame.mixer.music.stop()
            status = 'stopped'
        elif key == K_SPACE:
            pygame.mixer.music.unpause()
            status = 'playing'
    elif status == 'playing':
        if key == K_RETURN:
            pygame.mixer.music.stop()
            status = 'stopped'
        elif key == K_SPACE:
            pygame.mixer.music.pause()
            status = 'paused'



def draw_bars(h):
    bars = []
    for i in h:
        bars.append([len(bars) * WIDTH,50 + HEIGHT-i,WIDTH - 1,i])
    for i in bars:
        pygame.draw.rect(screen,[255,255,255],i,0)


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == KEYDOWN:
            controller(event.key)

    if num <= 0:
        status = 'stopped'

    name = my_font.render(file_name, True, (255,255,255))
    info = my_font.render(status.upper() + ' ' + get_time(), True, (255,255,255))
    screen.fill((0,0,0))
    screen.blit(name,(0, 0))
    screen.blit(info,(0, 18))
    fpsclock.tick(FPS)
    vis(status)

    pygame.display.update()

